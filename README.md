# Congress and Me (Phase 4)

| Name              | EID           | GitLabID           | Estimated Comp. | Actual Comp. |
| ------------------|:-------------:|:------------------:|:---------------:|:------------:|
| Abu-Zayd Abdullah | aa77266       | AbuZaydAbdullah    | 14 hours        | 20 hours     |
| Ben Burton        | btb896        | bentburton         | 10 hours        | 20 hours     |
| Calvin Dao        | cvd325        | auproxis           | 15 hours        | 20 hours     |
| Robert Gutierrez  | rg43274       | robgutierrez1      | 30 hours        | 20 hours     |
| Ben Hazel         | bgh559        | BenHazel007        | 15 hours        | 20 hours     |
| David Sikabwe     | dws2375       | dsikabwe           | 20 hours        | 20 hours     |


Git SHA: d0fa01deaed92199fa0d3f84be82842de5a47852<br>
Project Lead: David Sikabwe<br>
GitLab Pipelines: https://gitlab.com/AbuZaydAbdullah/congressconversation/pipelines<br>
Website: https://congressand.me<br>

## Available Commands

In the project directory, you can run:

### `npm install`

By default, npm install will install all modules listed as dependencies in package.json.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>